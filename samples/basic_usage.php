<?php
// initialize autoloader
require __DIR__ . '/Profiler.php';

// Teil 1
\Getdesigned\GdProfiler\GdProfiler::track('start #1');
\Getdesigned\GdProfiler\GdProfiler::sleep(1.5);

\Getdesigned\GdProfiler\GdProfiler::track('mitte #1');
\Getdesigned\GdProfiler\GdProfiler::sleep(1.5);

\Getdesigned\GdProfiler\GdProfiler::track('ende #1');


// Teil 2
\Getdesigned\GdProfiler\GdProfiler::track('start #2', 2);
\Getdesigned\GdProfiler\GdProfiler::sleep(1.5);

\Getdesigned\GdProfiler\GdProfiler::track('mitte #2', 2);
\Getdesigned\GdProfiler\GdProfiler::sleep(1.5);

\Getdesigned\GdProfiler\GdProfiler::track('ende #2', 2);

// Teil 2 an Teil 1 anhängen
$tracker = \Getdesigned\GdProfiler\GdProfiler::track(false, 2);
\Getdesigned\GdProfiler\GdProfiler::import($tracker);


// und noch ein einser extra
\Getdesigned\GdProfiler\GdProfiler::sleep(0.5);
\Getdesigned\GdProfiler\GdProfiler::track('endeplus #1');

echo \Getdesigned\GdProfiler\GdProfiler::output();

