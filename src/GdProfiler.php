<?php

declare(strict_types=1);
namespace Getdesigned\GdProfiler;

class GdProfiler
{
    protected static array $records = [];

    /**
     *
     * static method to track an event in the code
     * simply call with a string
     * returns an array of all records until now
     *
     * @param $input
     * @return array
     */
    public static function track($input = false, $tracker = 0)
    {
        $now = microtime(true);
        $memory = memory_get_usage();

        $duration = 0;
        $memoryIncrease = 0;

        if ($input) {
            static::$records[$tracker] = static::$records[$tracker] ?? [];

            if (sizeof(static::$records[$tracker]) > 0) {
                $lastRecord = static::$records[$tracker][sizeof(static::$records[$tracker]) - 1];

                if (is_array($input)) { // wir importieren
                    $now = $lastRecord['time'] + $input['duration+'];
                    $memory = $input['memory'];
                    $memoryIncrease = $input['memory+'];
                    $input = $input['text'];
                }
                $duration = $now - $lastRecord['time'];
                $memoryIncrease = $memory - $lastRecord['memory'];
            }

            static::$records[$tracker][] = [
                'time' => $now,
                'text' => $input,
                'duration+' => $duration,
                'duration' => $duration + ($lastRecord['duration'] ?? 0),
                'memory+' => $memoryIncrease,
                'memory' => $memory,
            ];
        }

        return self::getProfile($tracker);
    }

    /**
     * returns the profile data for a given tracker
     *
     * @param $tracker
     * @return mixed|null
     */
    public static function getProfile($tracker = 0)
    {
        return static::$records[$tracker] ?? null;
    }

    /**
     * @param $data array 2-dimensional array of profiling data to import
     * @param $tracker
     * @return void
     * @throws \Exception
     */
    public static function import($data, $tracker = 0)
    {
        if (!is_array($data) || !is_array($data[0])) {
            throw new \Exception('Invalid input');
        }
        // iterate over the data and add it to the records
        foreach ($data as $row) {
            self::track($row, $tracker);
        }
    }

    /**
     *
     * Sleep method capable of handling float values
     *
     * @param $float
     * @return void
     */
    public static function sleep($float) {
        $secs = (int) $float;
        $rest = $float - $secs;
        if ($secs > 0) {
            sleep($secs);
        }
        usleep($rest * 1000000);
    }

    /**
     *
     * Outputs a table of the profiling data
     *
     * @param $data string key of the profiling data to use
     * @param $hide array array of columns to hide
     * @return string
     * @throws Exception
     */
    public static function output($data = 0, $hide = ['time'], $format = 'auto')
    {
        // Gather data
        if (!is_array($data) && is_array(self::$records[$data])) {
            $data = self::$records[$data];
        } elseif (!is_array($data)) {
            throw new Exception('Invalid input');
        }

        // Remove hide columns
        $data = array_map(function ($row) use ($hide) {
            foreach ($hide as $key) {
                unset($row[$key]);
            }
            return $row;
        }, $data);

        // Limit columns with float values to 4 decimal places
        $data = array_map(function ($row) {
            foreach ($row as $key => $val) {
                if (is_float($val)) {
                    $row[$key] = number_format($val, 4);
                }
            }
            return $row;
        }, $data);

        // convert the columns memory and memory+ to human readable format with KB and MB
        $data = array_map(function ($row) {
            $row['memory'] = round($row['memory'] / 1024, 2) . " KB";
            $row['memory+'] = round($row['memory+'] / 1024, 2) . " KB";
            return $row;
        }, $data);

        // prepend the data array with one row where keys and values are the column names and uppercase the values
        $data = array_merge([array_combine(array_keys($data[0]), array_map('strtoupper', array_keys($data[0])))], $data);

        // Now output table
        if (($format === 'auto' && php_sapi_name() === 'cli') || $format === 'ascii') {
            // ASCII table
            $table = "";
            $max_col_lengths = [];

            // Get maximum column lengths
            foreach ($data as $row) {
                foreach ($row as $key => $val) {
                    $max_col_lengths[$key] = isset($max_col_lengths[$key]) ? max($max_col_lengths[$key], strlen($val)) : strlen($val);
                }
            }

            // Generate header row
            $table .= "+";
            foreach ($max_col_lengths as $length) {
                $table .= str_pad("", $length + 2, "-", STR_PAD_BOTH) . "+";
            }
            $table .= PHP_EOL;

            // Generate data rows
            foreach ($data as $row) {
                $table .= "|";
                foreach ($row as $key => $val) {
                    $table .= str_pad($val, $max_col_lengths[$key] + 2) . "|";
                }
                $table .= PHP_EOL;
            }

            // Generate bottom line
            $table .= "+";
            foreach ($max_col_lengths as $length) {
                $table .= str_pad("", $length + 2, "-", STR_PAD_BOTH) . "+";
            }
            $table .= PHP_EOL;

            return $table;
        } else if (($format === 'auto' && php_sapi_name() !== 'cli') || $format === 'html') {
            // HTML table
            $html = "<table>";
            // Generate data rows
            foreach ($data as $row) {
                $html .= "<tr>";
                foreach ($row as $val) {
                    $html .= "<td>" . htmlspecialchars((string)$val) . "</td>";
                }
                $html .= "</tr>";
            }

            $html .= "</table>";
            return $html;
        } else {
            throw new \Exception('Invalid format');
        }
    }
}